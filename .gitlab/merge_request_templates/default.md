<!-- If there is an option in the template, you need to fill in x in the square brackets in front of the option. -->

## Merge Request main content:
<!--Fill in the JIRA number and brief description of the modified issue below -->



## Change range
<!-- Select the scope of changes included in this MergeRequest and the options involved. Fill in x in []. After filling in x, there should be no spaces in []. Please delete the remaining options that are not involved. -->

- [ ] **Backstage**
   - [ ] API Changes `The corresponding Swagger has been updated`
   - [ ] Database Changes

- [ ] **front desk**
   - [ ] The last commit requires Build to pass

- [ ] **Integrated**
   - [ ] Microservice (modify/add microservice information, delete environment variables)
   - [ ] Build, Deploy related

- [ ] **Internationalized entries ([Feature].json)**
- [ ] **Internationalization entries ([Miscroservice].resx)**
- [ ] **Other (specify below)**

## Code Reviewers and key review content
<!-- Use @ to remind the reviewer and briefly describe the key content that needs to be reviewed
Reviewer and MR Approver cannot be the same person.-->
